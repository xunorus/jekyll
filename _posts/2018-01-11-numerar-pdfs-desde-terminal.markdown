---
layout: post
title: "numerar pdfs desde terminal"
date: "2018-01-11 15:58:08 -0300"
---
0. Instala pspdftool
1. Crear un archivo llamado addnumbers y poner este filtro dentro
    number(start 1 , size=20, x=297.5 pt, y=10 pt)
2. ./pspdftool -f addnumbers input.pdf output.pdf


#### source 
https://forum.ubuntu-it.org/viewtopic.php?t=485770

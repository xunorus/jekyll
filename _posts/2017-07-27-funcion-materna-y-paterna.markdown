---
layout: post
title: "funcion materna y paterna"
date: "2017-07-27 03:15:50 -0300"
---

La funcion materna lleva a la muerte.

La funcion paterna lleva a la vida.

Ambas son necesarias. Solo hay que entender cuando aplica.

Cuando uno descubre que la funcion de la vida es crecer, aparecera la madre para impedirlo.

# Las 3 herramientas de la funcion paterna
lo paterno tiene estas vias para dar un corte a la funcion materna.
1- la via de la prohibicion. El padre cuando prohibe protege.
2- La proteccion
3- Incentivacion

Cuando falla lo paterno en nuestra programacion siendo niños deriba en una PSICOSIS.
Si la funcion paterna fue ineficaz pero no tanto para devenir psicosis, deriva en una PERVERSION.
S la funcion paterna fue ejercida de la mejor manera deriva en una NEUROSIS (histérica u obsesiva)

La funcion materna permite el apego.
La funcion materna permite el desapego.
La clave es el factor temporal, el cuando ejercer cada una.

para entrar en sociedad he de ejercer una funcion materna. El problema es que quedamos apegado a las cosas que forman nuestro universo.
El tema es creer que eso a lo que estamos pegados es nuestro.
Pues cuando quieras alcanzar algo fuera de tu universo, no vas a poder. La unica forma de alcanzar algo fuera de mi universo es con la funcion paterna.

Las adicciones so ausencia de funcion paterna.

source [https://www.youtube.com/watch?v=Nb0XBvO0F-A]

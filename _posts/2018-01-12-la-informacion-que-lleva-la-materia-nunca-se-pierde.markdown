---
layout: post
title: "La informacion que lleva la materia nunca se pierde"
date: "2018-01-12 14:52:37 -0300"
---

El pensamiento no tiene masa, viaja mas rapido que la velocidad de la luz.

Los cuerpos no pueden porque tienen masa, y cuando se acerca a la velocidad de la luz su masa va creciendo por lo tanto no puede superarla.

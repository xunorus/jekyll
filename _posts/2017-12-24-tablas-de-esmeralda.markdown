---
layout: post
title: "tablas de esmeralda"
date: "2017-12-24 01:57:07 -0300"
---
I. Lo que digo no es ficticio, sino digno de crédito y cierto.

II. Lo que está más abajo es como lo que está arriba, y lo que está arriba es como lo que está abajo. Actúan para cumplir los prodigios del Uno.

III. Como todas las cosas fueron creadas por la Palabra del Ser, así todas las cosas fueron creadas a imagen del Uno.

IV. Su padre es el Sol y su madre la Luna. El Viento lo lleva en su vientre. Su nodriza es la Tierra.

V. Es el padre de la Perfección en el mundo entero.

VI. Su poder es fuerte si se transforma en Tierra.

VII. Separa la Tierra del Fuego, lo sutil de lo burdo, pero sé prudente y circunspecto cuando lo hagas.

VIII. Usa tu mente por completo y sube de la Tierra al Cielo, y, luego, nuevamente desciende a la Tierra y combina los poderes de lo que está arriba y lo que está abajo. Así ganarás gloria en el mundo entero, y la oscuridad saldrá de ti de una vez.

IX. Esto tiene más virtud que la Virtud misma, porque controla todas las cosas sutiles y penetra en todas las cosas sólidas.

X. Éste es el modo en que el mundo fue creado.

XI. Éste es el origen de los prodigios que se hallan aquí [¿o, que se han llevado a cabo?].

XII. Esto es por lo que soy llamado Hermes Trismegisto, porque poseo las tres partes de la filosofía cósmica.

XIII. Lo que tuve que decir sobre el funcionamiento del Sol ha concluido.


## referencias
– Éxodo 28:15-21
– Éxodo 39:8-14
– Génesis 29:32-35
– Génesis 30:1-13, 17-20
– Génesis 35:18
– Génesis 41:51-52
– Apocalipsis 21:19-21

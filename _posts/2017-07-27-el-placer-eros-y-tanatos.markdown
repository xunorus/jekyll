---
layout: post
title: "El placer: Eros y Tanatos"
date: "2017-07-27 02:44:07 -0300"
---

Lo placentero, sinonimo de agradable, viene de placenta. Cuanto estamos en la placenta de nuestra madre estamos placenteros.
Al momento del parto, nos desplazamos (displacer). De aquí surge la pulsion de muerte, el tanatos

# El mito
Tánatos era el hijo de Érebo y Nicte, hermano gemelo de Hipnos, y personificación de la muerte.

Era el genio alado que acudía a buscar los cuerpos de los que habían fallecido.

Cortaba un mechón de sus cabellos para ofrecer como tributo a Hades y se llevaba sus cuerpos al mundo de los muertos.

Transportó, ayudado por su hermano Hipnos, el cuerpo del guerrero Sarpedón, muerto en Troya, hasta Licia. También se llevó el cuerpo de Alcestis que, ejemplo del amor conyugal, había sustituido a su marido en el féretro.

Más tarde, su presa le fue arrebatada por Heracles, que lo obligó a devolverla a la vida más joven y más bella que nunca.

Sin embargo, la historia más curiosa en la que interviene Tánatos es en la que es encadenado por Sísifo.

Sísifo era el más astuto y el menos escrupuloso de los mortales. Era capaz de los más enrevesados engaños para conseguir sus propósitos.

Se dice que, al ser amante de Anticlea, él sería el verdadero padre de Ulises.

Cuando Zeus raptó a Egina, la hija del río Asopo, Sísifo fue testigo casual de los hechos. Utilizó la información para conseguir de Asopo un manantial en la ciudadela de Corinto, y delató a Zeus.

Éste, enfurecido, mandó a Tánatos para acabar con la vida del mortal, pero el hábil Sísifo consiguió atrapar y encadenar al genio alado de la muerte, y por un tiempo ningún hombre murió.

Finalmente, Ares liberó a Tánatos, que volvió a realizar su trabajo empezando por el propio Sísifo.

Pero Sísifo era capaz aun de más artimañas para librarse de la muerte, y antes de morir ordenó en secreto a su esposa que no le tributara honras fúnebres.

Una vez en los infiernos, se quejó ante Hades de la impiedad de su esposa y le pidió que le dejará volver para castigarla.

Hades se lo permitió y Sísifo, que no tenía intención de volver a los infiernos, vivió en la Tierra feliz hasta época muy avanzada. Cuando por fin murió, Hades le impuso una tarea para evitar una nueva evasión. Su martirio consistía en empujar cuesta arriba un gran peñasco que, una vez en la cumbre, volvía a caer por su propio peso y el trabajo de Sísifo se prolongaba así eternamente.

source:[http://mitosyleyendascr.com/mitologia-griega/tanatos/]

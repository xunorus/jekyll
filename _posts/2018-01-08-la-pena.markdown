---
layout: post
title: "la pena"
date: "2018-01-08 17:20:01 -0300"
---

Dar pena, es pedir al otro que reafirme mis creencias de separación.

Si yo doy pena o otro me da a mi, lo que pretendo es atarlo.
Lo mantengo atado a mi lado por pena. Y el que se pone enfermo es el que esta atrapado, porque esta haciendo algo que no quiere pero esta atrapado en que tiene que hacerlo.


Únete a la verdad de tu hermano, nunca te unas a sus sueño.

---
layout: post
title: "La conciencia"
date: "2018-01-11 15:14:18 -0300"
---
La conciencia

El pasado y el futuro son tiempos inexistentes.
El tiempo es espacio en movimiento, y el espacio es tiempo presente.
Si la conciencia lo incluye todo, y estoy viviendo con mi conciencia pensando en lo que me ocurrió y temiendo lo que me va a ocurrir, la conciencia no atina a encontrarme, pues nunca estoy en el sitio que debo de estar que es en el presente. Entonces cuando me viene la informacion yo o  me encuentro en el pasado, o en el futuro, y la conciencia siempre se manifiesta en el presente.

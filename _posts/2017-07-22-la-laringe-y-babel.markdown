---
layout: post
title: "La Laringe y Babel"
date: "2017-07-22 04:47:23 -0300"
---
Será que aquel pasaje que referencia "Bajemos y confundamos sus lenguas" se refiere a la inserción de la laringe en el ser humano. Un humano que sin la posibilidad de hablar se comunicaba perfectamente.

La laringe aparece de pronto en el hardware humano.

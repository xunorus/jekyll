var audioSources = [
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F1.mp3?alt=media&token=1fb1bea1-3341-46fc-b036-6fbc1528aaac',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F2.mp3?alt=media&token=fc59b1be-e9bd-4de1-b981-baaf635bdc15',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F3.mp3?alt=media&token=ed8bcbdc-9ddb-45eb-935b-f44499fe3bc4',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F4.mp3?alt=media&token=d1947b3e-1d00-4692-80b8-dbd1abee6efb',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F5.mp3?alt=media&token=d25659e2-6be5-497e-a31e-b336baea0a27',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F6.mp3?alt=media&token=f4149706-50d8-458d-83a9-445b1eee997e',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F7.mp3?alt=media&token=50e8e220-a2cc-433b-887b-425857ce6e5b',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F8.mp3?alt=media&token=e8a1c00c-183b-49ac-9625-1f0df5682e32',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F9.mp3?alt=media&token=5b5dce7e-e9f3-4123-8b6e-b652eade42af',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F10.mp3?alt=media&token=7ea73491-1fd7-406f-a29c-e229900e6d0e',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F11.mp3?alt=media&token=d2466e55-6d11-40f0-8b64-e2491ab462a3',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F12.mp3?alt=media&token=eb238d3b-e8b0-4ed5-ab7b-60484c81e3f0',
  'https://firebasestorage.googleapis.com/v0/b/cantata-instrumental.appspot.com/o/cantata%2F13.mp3?alt=media&token=f586e4ac-1dec-4f11-8083-8908095d4249'
]
var trackTittle = [
  'Huayno',
  'Cielos Nuevos',
  'Gato y Fuga',
  'Tahir',
  'Triunfo',
  'Zamba',
  'Chacarera',
  'Tríptico',
  'Baguala',
  'Bailecito',
  'Huella',
  'Litoraleña',
  'Cueca y Fuga'
]
var trackInfo = [
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet'
]

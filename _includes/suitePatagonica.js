var audioSources = [
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F1.preludioSideral.mp3?alt=media&token=49cd61b2-8c70-479a-aad9-71fffd9d6399',
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F2.tremoloDelVientoPuelche.mp3%2Bcroped.mp3?alt=media&token=b47406df-42da-4099-89fe-345a43bcde46',
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F3.huaynoDelCamino.mp3?alt=media&token=bf9ce9a2-7cf8-443d-9475-d920cbd7fff9',
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F4.loncomeoDelAlmayLasPenas.mp3?alt=media&token=735f58b6-f16c-4f29-94e1-e8f72a62f434',
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F5.chacareraDeLaTribu.mp3?alt=media&token=b66c1a7a-1360-4cfe-9646-cd34adcd4d60',
'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F6.postludioSideral.mp3?alt=media&token=42a6c916-0bf2-49fb-b66b-ae6c8e23e260'
]
var trackTittle = [
  'Preludio Sideral',
  'Trémolo del Viento Puelche',
  'Huayno del Camino',
  'Loncomeo del Alma y las Penas',
  'Chacarera de la Tribu',
  'Postludio Sideral'
]
var trackInfo = [
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet',
  'feat. FC Patagonia String Quartet'
]

// ===== Open Nav =====
$( ".burger-wrapper" ).click(function() {

	// ===== If Nav is not open
	if($('.nav').css("display") == "none"){
		TweenMax.to(".dim", 0.5, {opacity: 1, display: 'block', ease: Power2.easeInOut});
		TweenMax.fromTo(".nav", 0.5, {xPercent: -100},
									{xPercent: 0, display: 'block', ease: Expo.easeOut});
		TweenMax.staggerFrom('.nav li', 0.5, {opacity:0, y: 20, ease: Power2.easeInOut}, 0.1);

		$('.logo-text').css({'opacity': '0', 'display': 'none'});
		// $('#ac_background').css({'filter': 'blur(5px)'});
		// $('#ac_background').toggleClass('on');

  }
	// ===== If Nav is open	and in Curation page
	else if($('.nav').css("display") == "block" && $('#curator').css("display") == "block"){
 		TweenMax.to(".dim", 0.5, {opacity: 0, display: 'none', ease: Power2.easeInOut});
		TweenMax.to(".nav", 0.5, {xPercent: -100, display:'none', ease: Expo.easeOut});
		// $('.logo-text').css({'opacity': '1', 'display': 'block'});

// console.log('Nav is open	and in Curation page');
  }

	else {
	  TweenMax.to(".dim", 0.5, {opacity: 0, display: 'none', ease: Power2.easeInOut});
		TweenMax.to(".nav", 0.5, {xPercent: -100, display:'none', ease: Expo.easeOut});
		$('.logo-text').css({'opacity': '1', 'display': 'block'});
		// $('#ac_background').css({'filter': 'blur(0px)'});
	}

});


// ===== Open Player + dim on =====

// $( ".btn-open-player, .track_info" ).click(function() {
// $( ".btn-open-player" ).click(function() {
//   TweenMax.to(".dim", 0.5, {opacity: 1, display: 'block', ease: Power2.easeInOut});
// 	TweenMax.fromTo("#player", 0.5, {xPercent: 100},
// 									{xPercent: 0, display: 'block', ease: Expo.easeOut});
// 	TweenMax.to(".mini-player", 0.5, {x: 50, ease: Expo.easeOut});
// });

$('.dim').click(function() {
	TweenMax.to(".dim", 0.5, {opacity: 0, display: 'none', ease: Power2.easeInOut});
	// TweenMax.to("#player", 0.5, {xPercent: 100, display: 'none', ease: Expo.easeOut});
	TweenMax.to(".nav", 0.5, {xPercent: -100, display: 'none', ease: Power2.easeInOut})
	TweenMax.to(".mini-player", 0.5, {x: 0, ease: Expo.easeOut});
	// toggleLoop();
	// var loopIcon = document.getElementById("loopIcon");
	// loopIcon.className = "icon-music";
	loopIcon.className = "icon-indent-right";


});

// ===== Mini Player - Play/Pause Switch =====

// $('.btn-play').click(function(){
// 	TweenMax.to($('.btn-play'), 0.2, {x: 20, opacity: 0, scale: 0.3,  display: 'none', ease: Power2.easeInOut});
// 	TweenMax.fromTo($('.btn-pause'), 0.2, {x: -20, opacity: 0, scale: 0.3, display: 'none'},
// 								 {x: 0, opacity: 1, scale: 1, display: 'block', ease: Power2.easeInOut});
// });

// $('.btn-pause').click(function(){
// 	TweenMax.to($('.btn-pause'), 0.2, {x: 20, opacity: 0, display: 'none', scale: 0.3, ease: Power2.easeInOut});
// 	TweenMax.fromTo($('.btn-play'), 0.2, {x: -20, opacity: 0, scale: 0.3, display: 'none'},
// 								 {x: 0, opacity: 1, display: 'block', scale: 1, ease: Power2.easeInOut});
// });

// ===== HoverIn/HoverOut Flash Effect =====

$('.track_info').hover(function(){

	TweenMax.fromTo($(this), 0.5, {opacity: 0.5, ease: Power2.easeInOut},
								 {opacity: 1})},
	function(){
		$(this).css("opacity", "1");
});

$('.burger-wrapper, .logo-text, .back_btn').hover(function(){

	TweenMax.fromTo($(this), 0.5, {opacity: 0.5, ease: Power2.easeInOut},
								 {opacity: 1})},
	function(){
		$(this).css("opacity", "1")
});

$('.btn-open-player').hover(function(){

	TweenMax.fromTo($(this), 0.5, {opacity: 0.5, ease: Power2.easeInOut},
								 {opacity: 1})},
	function(){
		$(this).css("opacity", "1")
});

$('.nav a').hover(function(){

	TweenMax.fromTo($(this), 0.5, {opacity: 0.5, ease: Power2.easeInOut},
								 {opacity: 1})},
	function(){
		$(this).css("opacity", "1")
});
// ===================================
// ===== Player List Items =====
// ===================================
$(document).on("click", ".list_item",function() {

	$('.list_item').removeClass('selected');
	$(this).addClass('selected');
	var src = $(this).find('source').attr('src');
	var currentTrackTittle = $(this).find('.title').text();
	var currentTrackInfo = $(this).find('.artist').text();
	console.log('clicked' + currentTrackInfo);
	//
	$('#info').empty();
  document.getElementById("info").innerHTML = "<h1 class='afterText'>" + currentTrackTittle + "</h1>";
  $("#info").append("<h2 class='afterText'>" + currentTrackInfo + "</h2>");
	$(".afterText").textEffect();

	var totalTime = document.getElementById("totalTime");

	a.resetSong();
	a.song = new Audio(src);
	a.song.duration;
	a.play();
	a.song.onloadeddata = function() {
var tt =		a.song.duration;
totalTime.innerHTML = formatTime(tt);

// console.log(tt);
};
});


// ===== Main Play Button - Hover =====

$('.text-wrap .text').hover(function(){
	TweenMax.to($('.main-btn_wrapper'), 0.5, {opacity: 1, display: 'block', position: 'absolute', scale: 1, ease: Elastic.easeOut.config(1, 0.75)}),
	TweenMax.to($('.line'), 0.5, {css: { scaleY: 0.6, transformOrigin: "center center" }, ease: Expo.easeOut})},

	function(){
		TweenMax.to($('.main-btn_wrapper'), 0.5, {opacity: 0, display: 'none', scale: 0, ease: Elastic.easeOut.config(1, 0.75)}),
		TweenMax.to($('.line'), 0.5, {css: { scaleY: 1, transformOrigin: "center center" }, ease: Expo.easeOut})
});


// ===== CDs Curation Page  =====
// ===== List  =====
$('.item').hover(function(){
	TweenMax.to($(this), 0.5, {y: -30, ease: Power2.easeInOut}),
	$(this).children('.thumb').addClass('shadow'),
	$(this).children('.connect_btn').addClass('shadow'),

	TweenMax.to($(this).children('.info'), 0.5, {opacity: 1, ease: Power2.easeInOut})
	},

	function(){
		TweenMax.to($(this), 0.5, {y: 0, ease: Power2.easeInOut}),
		$(this).children('.thumb').removeClass('shadow'),
		$(this).children('.connect_btn').removeClass('shadow'),

		TweenMax.to($(this).children('.info'), 0.5, {opacity: 0, ease: Power2.easeInOut})
});


// ===== Home Page to Curation Page Transition  =====
// ===== Main Play Button Activate =====

// $('.text-wrap .text').click(function(){
$('.text-wrap .text').click(function(){

// alert('clickeado');
	var homeToMain = new TimelineMax({});

	// Hide
	$('.logo-text').css('display', 'none'),
	homeToMain.to($('.line, .text-wrap'), 0.5, {display: 'none', opacity: 0, y: -20, ease: Power2.easeInOut}, 0),

	// Background down
	homeToMain.to($('.wave-container'), 1, {yPercent: 30, ease: Power2.easeInOut}, 0),


	// Show
	$('#curator').css('display', 'block'),
	homeToMain.fromTo($('.back_btn'), 0.8, {x: 15},
										{display: 'flex', opacity: 1, x: 0, ease: Power2.easeInOut}, 1),

	homeToMain.fromTo($('.curator_title_wrapper'), 0.8, {opacity: 0, x: 30},
										{opacity: 1, x: 0, ease: Power2.easeInOut}, 1),

	homeToMain.fromTo($('.curator_list'), 0.8, {opacity: 0, display: 'none', x: 30},
									{opacity: 1, x: 0, display: 'block', ease: Power2.easeInOut}, 1.2)


//blur
$('#ac_background').addClass('off');
// $('#ac_background').css({'filter': 'blur(5px)'});


});


// ===== Curation Page to Playlist Page Transition  =====
// ===== Item Activate =====
$('.item').click(function(){
	var mainToPlaylist = new TimelineMax({});

// var $itemClicked = $this;
// console.log($itemClicked);
console.log($(this).text());
	// Hide
	mainToPlaylist.to($('#curator'), 0.8, {display: 'none', opacity: 0, scale: 1.1, onComplete:completeHandler, ease: Power2.easeInOut}, 0)

function completeHandler() {
	$('#player').stop(true).animate({'bottom':'2%' },600,'easeOutBack');
	console.log('mainToPlaylist');
}

});

// ===== Back Button Activate =====

$('.back_btn').click(function(){
// ===== From Playlist(3) to Main(2)
	if($('#curator').css("display") == "none"){
		var playlistToMain = new TimelineMax({});
		console.log('playlistToMain');

		// Hide
		playlistToMain.fromTo($('#curator'), 0.8, {display: 'none', opacity: 0, scale: 1.1},
											{display: 'block', opacity: 1, scale: 1, ease: Power2.easeInOut}, 0)


	}

	// From Main(2) to Home(1)
	else {
		$('#ac_background').removeClass('off');
		var mainToHome = new TimelineMax({});
		// Hide
		mainToHome.fromTo($('.curator_title_wrapper'), 0.5, {opacity: 1, x: 0},
											{opacity: 0, x: 30, ease: Power2.easeInOut}, 0.2),

		mainToHome.fromTo($('.curator_list'), 0.5, {opacity: 1, display: 'block', x: 0},
											{opacity: 0, x: 30, display: 'none', ease: Power2.easeInOut}, 0.5),


		mainToHome.to($('.back_btn'), 0.5, {display: 'none', opacity: 0, x: 15, ease: Power2.easeInOut}, 0.5),

		mainToHome.to($('#curator'), 0, {display: 'none', ease: Power2.easeInOut}, 1),

		// Background Up
		mainToHome.to($('.wave-container'), 1, {yPercent: 0, ease: Power2.easeInOut}, 1),

		// 	Show
		mainToHome.to($('.text-wrap'), 0.5, {display: 'flex', opacity: 1, y: 0, ease: Power2.easeInOut}, 1.2),

		mainToHome.to($('.logo-text, .line'), 0.5, {display: 'block', opacity: 1, y: 0, ease: Power2.easeInOut}, 1.2),

		// 	Force to redraw by using y translate
		mainToHome.fromTo($('.text-wrap .text'), 0.1, {y: 0.1, position: 'absolute'},
											{y: 0, position: 'relative', ease: Power2.easeInOut, onComplete:completeHandler}, 1.3)
		// $('.text-wrap .text').css('position', 'relative');

/*********************************************************************************************
.)    completeHandler
**********************************************************************************************/

// alert('from main to home')
function completeHandler() {
	$('#player').stop(true).animate({'bottom':'2%' },600,'easeOutBack');


// $('.ac_subitem').stop().animate({	height		: '100%',	top : '0%'}, 400, function() {	showItemImage(el_image);});

$ac_content		= $('#ac_content'),
$title			= $ac_content.find('h1'),
$menu			= $ac_content.find('.ac_menu'),
$mainNav		= $menu.find('ul:first'),
$menuItems		= $mainNav.children('li'),
$menuItems		= $mainNav.children('li'),
totalItems		= $menuItems.length;

/* shows / hides the menu items */
toggleMenuItems		= function(dir) {
return $.Deferred(
function(dfd) {
	/*
	slides in / out the items.
	different animation time for each one.
	*/
	$menuItems.each(function(i) {
				var $el_title	= $(this).children('a:first'),
					marginTop, opacity, easing;
				if(dir === 'up'){
					marginTop	= '0px';
					opacity		= 1;
					easing		= 'easeOutBack';
				}
				else if(dir === 'down'){
					marginTop	= '60px';
					opacity		= 0;
					easing		= 'easeInBack';
}
		$el_title.stop()
		.animate({
							marginTop	: marginTop,
							opacity		: opacity
						 }, 200 + i * 200 , easing, function(){
			if(i === totalItems - 1)
				dfd.resolve();
		});
	});
}
).promise();
};

toggleMenuItems('up');
$('a.navlinks').show();






	console.log('mainToPlaylist');
}
	}
});

 /*********************************************************************************************
     .)    volument effect
     **********************************************************************************************/

 // jQuery
 $('#mute').hover(
   function() {
     $('#volumen').css({
       'height': '101px',
       'top': '-97px'
     })
   },
   function() {
     $('#volumen').css({
       'height': '5px',
       'top': '0px'
     })
   }
 );


 /*********************************************************************************************
  .)    PLAYER
  **********************************************************************************************/

 function AudioPlayer(_autoPlay, _loop, _function) {
   var visualization = document.getElementById("visualization");
   var stage;
   var playBtn = document.getElementById("play");
   var currentTime = document.getElementById("currentTime");
   var totalTime = document.getElementById("totalTime");
   var seekBg = document.getElementById("seek");
   var seekHolder = document.getElementById("seekHolder");
   var seekFill = document.getElementById("seekFill");
   var seekDrag = document.getElementById("seekDrag");
   var muteBtn = document.getElementById("mute");
   var volumeIcon = document.getElementById("volumeIcon");
   var volumeOverlay = document.getElementById("volumeOverlay");
   var volumeFill = document.getElementById("volumeFill");
   var volumePercent = document.getElementById("volumePercent");
   var loopBtn = document.getElementById("loop");
   var loopIcon = document.getElementById("loopIcon");
   var timeLabel = document.getElementById("timeLabel");
   var oldVolume = 1;
   var context;
   var analyser;
   var source;
   var animationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;
   this.volume = 1;
   this.loop = _loop;
   this.autoPlay = _autoPlay;
   var $base = window.location.origin;
   var isMobile = false; //initiate as false
   // device detection
   if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
     isMobile = true;


   //  var audioSources = [
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F1.preludioSideral.mp3?alt=media&token=49cd61b2-8c70-479a-aad9-71fffd9d6399',
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F2.tremoloDelVientoPuelche.mp3%2Bcroped.mp3?alt=media&token=b47406df-42da-4099-89fe-345a43bcde46',
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F3.huaynoDelCamino.mp3?alt=media&token=bf9ce9a2-7cf8-443d-9475-d920cbd7fff9',
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F4.loncomeoDelAlmayLasPenas.mp3?alt=media&token=735f58b6-f16c-4f29-94e1-e8f72a62f434',
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F5.chacareraDeLaTribu.mp3?alt=media&token=b66c1a7a-1360-4cfe-9646-cd34adcd4d60',
   //  'https://firebasestorage.googleapis.com/v0/b/suite-patagonica.appspot.com/o/suitePatagonica%2F6.postludioSideral.mp3?alt=media&token=42a6c916-0bf2-49fb-b66b-ae6c8e23e260'
   //  ]

   albumsXunorus = {
     "suite_patagonica": [{
         "track": "1",
         "title": "Preludio Sideral",
         "source": "../../../cds/mp3/suitePatagonica/1.preludioSideral.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       },
       {
         "track": "2",
         "title": "Trémolo del Viento Puelche",
         "source": "../../../cds/mp3/suitePatagonica/2.tremoloDelVientoPuelche.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       },
       {
         "track": "3",
         "title": "Huayno del Camino",
         "source": "../../../cds/mp3/suitePatagonica/3.huaynoDelCamino.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       },
       {
         "track": "4",
         "title": "Loncomeo del Alma y las Penas",
         "source": "../../../cds/mp3/suitePatagonica/4.loncomeoDelAlmayLasPenas.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       },
       {
         "track": "5",
         "title": "Chacarera de la Tribu",
         "source": "../../../cds/mp3/suitePatagonica/5.chacareraDeLaTribu.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       },
       {
         "track": "6",
         "title": "Postludio Sideral",
         "source": "../../../cds/mp3/suitePatagonica/6.postludioSideral.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/suitePatagonica100.jpg",
       }
     ],
     "coleccion": [{
         "track": "1",
         "title": "Momento I",
         "source": "../../../cds/mp3/coleccion/1.momentoI.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "2",
         "title": "Trolope",
         "source": "../../../cds/mp3/coleccion/2.trolope.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "3",
         "title": "Momento II",
         "source": "../../../cds/mp3/coleccion/3.momentoII.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "4",
         "title": "El Carnero",
         "source": "../../../cds/mp3/coleccion/4.elCarnero.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "5",
         "title": "Momento III",
         "source": "../../../cds/mp3/coleccion/5.momentoIII.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "6",
         "title": "Cimbronazo",
         "source": "../../../cds/mp3/coleccion/6.cimbronazo.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "7",
         "title": "Momento IV",
         "source": "../../../cds/mp3/coleccion/7.momentoIV.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "8",
         "title": "La Danza del Girasol",
         "source": "../../../cds/mp3/coleccion/8.laDanzaDelGirasol.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "9",
         "title": "Momento V",
         "source": "../../../cds/mp3/coleccion/9.momentoV.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "10",
         "title": "Soledad",
         "source": "../../../cds/mp3/coleccion/10.soledad.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "11",
         "title": "Angostura",
         "source": "../../../cds/mp3/coleccion/11.angostura.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "12",
         "title": "Momento VI",
         "source": "../../../cds/mp3/coleccion/12.momentoVI.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       },
       {
         "track": "13",
         "title": "Luna sobre el Nahuel ",
         "source": "../../../cds/mp3/coleccion/13.lunaSobreElNahuel.mp3",
         "info": "feat. FC Patagonia String Quartet",
         "cover": "/assets/cds/coleccion100.jpg",
       }
     ]
   }

   var audioSources = [
     '../../../cds/mp3/suitePatagonica/1.preludioSideral.mp3',
     '../../../cds/mp3/suitePatagonica/2.tremoloDelVientoPuelche.mp3',
     '../../../cds/mp3/suitePatagonica/3.huaynoDelCamino.mp3',
     '../../../cds/mp3/suitePatagonica/4.loncomeoDelAlmayLasPenas.mp3',
     '../../../cds/mp3/suitePatagonica/5.chacareraDeLaTribu.mp3',
     '../../../cds/mp3/suitePatagonica/6.postludioSideral.mp3'
   ]
   var trackTittle = [
     'Preludio Sideral',
     'Trémolo del Viento Puelche',
     'Huayno del Camino',
     'Loncomeo del Alma y las Penas',
     'Chacarera de la Tribu',
     'Postludio Sideral'
   ]
   var trackInfo = [
     'feat. FC Patagonia String Quartet',
     'feat. FC Patagonia String Quartet',
     'feat. FC Patagonia String Quartet',
     '3feat. FC Patagonia String Quartet',
     'feat. FC Patagonia String Quartet',
     'feat. FC Patagonia String Quartet'
   ]

   var trackImg = [
     '/assets/cds/suitePatagonica100.jpg',
     '/assets/cds/suitePatagonica100.jpg',
     '/assets/cds/suitePatagonica100.jpg',
     '/assets/cds/suitePatagonica100.jpg',
     '/assets/cds/suitePatagonica100.jpg',
     '/assets/cds/suitePatagonica100.jpg'
   ]

   var randomNumber = Math.floor(Math.random() * (audioSources.length));
   this.song = new Audio(audioSources[randomNumber]);
   var currentTrackTittle = trackTittle[randomNumber];
   var currentTrackInfo = trackInfo[randomNumber];
   var currentTrackPath = audioSources[randomNumber];
   var currentTrackImg = trackImg[randomNumber];
   var currentSource = audioSources[randomNumber];


   if (isMobile == true) {
     /*********************************************************************************************
     0.)   mobile
     **********************************************************************************************/
     $(document).ready(function($) {
       setTimeout(function() {
         $('#info').empty();
         document.getElementById("info").innerHTML = "<h1 class='afterText'>" + currentTrackTittle + "</h1>";

         // PLAYLIST
         var $playlist = $('#playlist');
         $playlist.empty();
         var x = albumsXunorus.suite_patagonica;
         var x = albumsXunorus.coleccion;
         for (i in x) {
           $playlist.append("<li class='list_item'>" + "<div class='thumb'  style='background: url(" +
             x[i].cover + " ')'> </div>" + "<source src=" + '"' +
             x[i].source + '"' + "type='audio/mpeg'/>" + "<div class='info'>" + "<div class='title'>" +
             x[i].title + "</div>" + "<div class='artist'>" +
             x[i].info + "</div>" + "</div>" + "</li>");
         }
         $(".afterText").textEffect();
       }, 2500);
     });
   }
   if (isMobile == false) {
     /*********************************************************************************************
     1.)   desktop
     **********************************************************************************************/
     $(document).ready(function($) {

       setTimeout(function() {
         $('#info').empty();
         document.getElementById("info").innerHTML = "<h1 class='afterText'>" + currentTrackTittle + "</h1>";
         $("#info").append("<h2 class='afterText'>" + currentTrackInfo + "</h2>");
         // PLAYLIST
         var $playlist = $('#playlist');
         $playlist.empty();
        //  var x = albumsXunorus.suite_patagonica;
         var x = albumsXunorus.coleccion;
         for (i in x) {
           $playlist.append("<li class='list_item'>" + "<div class='thumb'  style='background: url(" +
             x[i].cover + " ')'> </div>" + "<source src=" + '"' +
             x[i].source + '"' + "type='audio/mpeg'/>" + "<div class='info'>" + "<div class='title'>" +
             x[i].title + "</div>" + "<div class='artist'>" +
             x[i].info + "</div>" + "</div>" + "</li>");
         }
         $(".afterText").textEffect();
       }, 2500);

     });
   }


   this.playing = false;
   this.width = 590;
   this.height = 290;
   this.play = function() {
     playBtn.innerHTML = "<i class='icon-pause'></i>";
     this.playing = true;
     this.song.play();
   };

   this.pause = function() {
     playBtn.innerHTML = "<i class='icon-play'></i>";
     this.playing = false;
     this.song.pause();
   };

   var togglePlay = function(event) {
     if (event) event.preventDefault();
     if (this.song.paused) {
       this.play();
     } else {
       this.pause();
     }
   }.bind(this);

   ////////////////////////////////////////////////////////////
   /**
    * User clicks on a song in the playlist. Plays it
    */
   // function clickSong() {
   //   var index = $(this).parent().index();
   //   playSong(index + 1);
   //   return false;
   // }
   //
   // /**
   //  * plays song in position i of the list of songs (playlist)
   //  */
   // function playSong(i) {
   //   var $next_song = $list.find('li:nth-child(' + i + ')');
   //   var mp3 = $next_song.find('.mp_mp3').html();
   //   var ogg = $next_song.find('.mp_ogg').html();
   //   $list.find('.jplayer_playlist_current').removeClass("jplayer_playlist_current");
   //   $next_song.find('a').addClass("jplayer_playlist_current");
   //   $("#jquery_jplayer").jPlayer("setFile", mp3, ogg).jPlayer("play");
   // }

   ////////////////////////////////////////////////////////////

   document.onkeydown = function(evt) {
     if (evt.keyCode == 32) {
       togglePlay();

     }
   };
   //fin  INSERCION KEYUP

  //  var formatTime = function(i) {

    formatTime = function(i) {
     var minutes = Math.floor(i / 60);
     var seconds = Math.floor(i % 60);
     return ((minutes < 10) ? ("0" + minutes) : minutes) + ":" + ((seconds < 10) ? ("0" + seconds) : seconds);
   };

   var getMousePos = function(evt, element) {
     var rect = element.getBoundingClientRect();
     var root = document.documentElement;

     var mouseX = evt.clientX - rect.left - root.scrollLeft;
     var mouseY = evt.clientY - rect.top - root.scrollTop;

     return {
       x: mouseX,
       y: mouseY
     };
   }.bind(this);

   var changeTime = function(event) {
     event.preventDefault();
     this.song.addEventListener("timeupdate", updateTime, false);
     document.removeEventListener("mousemove", seek, false);
     document.removeEventListener("mouseup", changeTime, false);
     event.preventDefault();
     var mousePos = getMousePos(event, seekBg);
     this.seekTo((mousePos.x / seekBg.offsetWidth) * this.song.duration);
     return false;
   }.bind(this);

   var startSeek = function(event) {
     event.preventDefault();
     this.song.removeEventListener("timeupdate", updateTime, false);
     document.addEventListener("mousemove", seek, false);
     document.addEventListener("mouseup", changeTime, false);
     return false;
   }.bind(this);

   var seek = function(event) {
     event.preventDefault();
     var mousePos = getMousePos(event, seekBg);
     var dragTime = (mousePos.x / seekBg.offsetWidth) * this.song.duration;
     timeLabel.innerHTML = formatTime(dragTime);
     if (Math.round((1 - (dragTime / this.song.duration)) * (seekBg.offsetWidth)) + 3 < timeLabel.offsetWidth / 2) {
       timeLabel.style.left = (seekHolder.offsetWidth - timeLabel.offsetWidth) + "px";
     } else if (Math.round(((dragTime / this.song.duration)) * (seekBg.offsetWidth) + 3) < timeLabel.offsetWidth / 2) {
       timeLabel.style.left = "0px";
     } else {
       timeLabel.style.left = Math.round(((dragTime / this.song.duration)) * (seekBg.offsetWidth) + 3 - timeLabel.offsetWidth / 2) + "px";
     }
     seekFill.style.width = Math.round((dragTime / this.song.duration) * (seekBg.offsetWidth - 2)) + "px";
     seekDrag.style.left = Math.round((dragTime / this.song.duration) * (seekBg.offsetWidth - 2)) + "px";
     return false;
   }.bind(this);

   var updateTime = function(event) {
     currentTime.innerHTML = formatTime(this.song.currentTime);
     timeLabel.innerHTML = currentTime.innerHTML;
     if (Math.round((1 - (this.song.currentTime / this.song.duration)) * (seekBg.offsetWidth)) + 3 < timeLabel.offsetWidth / 2) {
       timeLabel.style.left = (seekHolder.offsetWidth - timeLabel.offsetWidth) + "px";
     } else if (Math.round(((this.song.currentTime / this.song.duration)) * (seekBg.offsetWidth) + 3) < timeLabel.offsetWidth / 2) {
       timeLabel.style.left = "0px";
     } else {
       timeLabel.style.left = Math.round(((this.song.currentTime / this.song.duration)) * (seekBg.offsetWidth) + 3 - timeLabel.offsetWidth / 2) + "px";
     }
     seekFill.style.width = Math.round((this.song.currentTime / this.song.duration) * (seekBg.offsetWidth - 2)) + "px";
     seekDrag.style.left = Math.round((this.song.currentTime / this.song.duration) * (seekBg.offsetWidth - 2)) + "px";
   }.bind(this);

   this.turnOffLoop = function() {
     //  this.loop = false;
     //  this.song.loop = false;
     loopIcon.className = "icon-indent-right";
   };

   this.turnOnLoop = function() {
     //  this.loop = true;
     //  this.song.loop = true;
     loopIcon.className = "icon-music";
   };

   var toggleLoop = function(event) {
     if (event) event.preventDefault();
     if (this.loop) {
       this.turnOffLoop();
     } else {
       this.turnOnLoop();
     }
   }.bind(this);

   this.seekTo = function(time) {
     this.song.currentTime = time;
   };

   this.resetSong = function(event) {
     if ((event && !this.loop) || !event) {
       this.seekTo(0);
       this.pause();
       console.log('termino la obra');
     }
   }.bind(this);

   this.changeVolume = function(volume) {
     if (volume !== 0) oldVolume = volume;
     this.volume = volume;
     this.song.volume = volume;
   };

   this.mute = function() {
     oldVolume = this.volume;
     this.changeVolume(0);
   };

   this.unMute = function() {
     this.changeVolume(oldVolume);
   };

   var toggleMute = function(event) {
     if (this.volume === 0) {
       this.unMute();
     } else {
       this.mute();
     }
   }.bind(this);

   var updateVolume = function(event) {
     volumePercent.innerHTML = Math.round(this.volume * 100);
     volumeFill.style.height = Math.round(this.volume * 70) + "px";
     if (this.volume === 0) {
       volumeIcon.className = "icon-volume-off";
     } else if (Math.round(this.volume) === 0) {
       volumeIcon.className = "icon-volume-down";
     } else {
       volumeIcon.className = "icon-volume-up";
     }
   }.bind(this);

   var startDragVolume = function(event) {
     event.preventDefault();
     muteBtn.removeEventListener("mousedown", startDragVolume, false);
     document.addEventListener("mousemove", changeVolumeSlider, false);
     document.addEventListener("mouseup", dropVolumeSlider, false);
     return false;
   }.bind(this);

   var dropVolumeSlider = function(event) {
     event.preventDefault();
     muteBtn.addEventListener("mousedown", startDragVolume, false);
     document.removeEventListener("mousemove", changeVolumeSlider, false);
     document.removeEventListener("mouseup", dropVolumeSlider, false);
     return false;
   }.bind(this);

   var changeVolumeSlider = function(event) {
     event.preventDefault();
     var mousePos = getMousePos(event, volumeOverlay);
     var newVolume = (volumeOverlay.offsetHeight - mousePos.y) / volumeOverlay.offsetHeight;
     if (newVolume > 1) newVolume = 1;
     if (newVolume <= 0) {
       toggleMute();
     } else {
       this.changeVolume(newVolume);
     }
     return false;
   }.bind(this);



   var init = function() {
     totalTime.innerHTML = formatTime(this.song.duration);


     if (this.autoPlay) {
       this.play();
     }

     playBtn.addEventListener("click", togglePlay, false);
     seekBg.addEventListener("mousedown", startSeek, false);
     seekFill.addEventListener("mousedown", startSeek, false);
     seekDrag.addEventListener("mousedown", startSeek, false);
     loopBtn.addEventListener("click", toggleLoop, false);
     muteBtn.addEventListener("mousedown", startDragVolume, false);
     muteBtn.addEventListener("click", changeVolumeSlider, false);
     this.song.addEventListener("timeupdate", updateTime, false);
     this.song.addEventListener("ended", this.resetSong, false);
     this.song.addEventListener("volumechange", updateVolume, false);
   }.bind(this);


   this.song.addEventListener("canplaythrough", init, false);
   if (this.loop) {
     this.turnOnLoop();
   } else {
     this.turnOffLoop();
     console.log('loop apagado');
   }
 }


a = new AudioPlayer(false, false);
// a = new AudioPlayer(false, false);
 // var a = new AudioPlayer(false, false, bars);

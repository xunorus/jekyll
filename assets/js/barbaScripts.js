// Please note, the DOM should be ready
window.fx1 = "transition.slideRightIn"; // efectos de velocity
window.fx2 = "transition.slideLeftIn"; // efectos de velocity
window.fx3 = "transition.slideDownIn"; // efectos de velocity
window.fx4 = "transition.slideUpIn"; // efectos de velocity


var divs = document.getElementById('barba-wrapper');

Barba.Pjax.start();
Barba.Dispatcher.on('newPageReady',
  function(currentStatus, oldStatus, container) {
    $(divs).velocity(fx1);


  });



/************************************************
	LANDING
************************************************/

var Landingpage = Barba.BaseView.extend({
  namespace: 'landing',
  onEnter: function() {
    console.log("landing");
  }
});
Landingpage.init();


/************************************************
	loadScript
************************************************/

function loadScript(url) {
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = url;
  head.appendChild(script);
}

function aud_play_pause() {
  var myAudio = document.getElementById("myTune");
  var myAudioIcon = document.getElementById("stateicon");

  if (myAudio.paused) {
    myAudioIcon.classList.remove('fa-play');
    myAudioIcon.classList.add('fa-pause');

    myAudio.play();
  } else {
    myAudioIcon.classList.remove('fa-pause');
    myAudioIcon.classList.add('fa-play');

    myAudio.pause();
  }
}


var str1 = window.location.origin;
var scr0 = str1.concat("/js/main.js");
